import unittest
import service


class TestServiceMethods(unittest.TestCase):
    def test_ismail(self):
        self.assertTrue(service.is_mail("test@ya.ru"))
        self.assertFalse(service.is_mail("teststing"))

    def test_is_date(self):
        self.assertTrue(service.is_date("21-02-2017"))
        self.assertTrue(service.is_date("2017.02.21"))
        self.assertFalse(service.is_date("2102-2017"))
        self.assertFalse(service.is_date("2017.02.121213"))

    def test_isphone(self):
        self.assertTrue(service.is_phone("+79601111111"))
        self.assertFalse(service.is_phone("2132335sf"))


if __name__ == "__main__":
    unittest.main()

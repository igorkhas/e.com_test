from re import match, compile, sub
from time import strptime
from flask import Flask, request
from tinydb import TinyDB, Query

app = Flask(__name__)
db = TinyDB("./db.json")
template = Query()


@app.route("/", methods=["POST"])
def index():
    # Create empty dict
    field_types = {}
    for value in request.form.values():
        if is_date(value) != False:
            field_types["date"] = value
        elif is_phone(value) != False:
            field_types["phone"] = value
        elif is_mail(value) != False:
            field_types["email"] = value
        else:
            if len(value) > 0:
                field_types["text"] = value

                # result = db.search(template.date)


                # Debug purpose
    print(field_types)
    return type(result)


def is_mail(email_adress):
    email_pattern = compile("[^@]+@[^@]+\.[^@]+")
    if email_pattern.match(email_adress) != None:
        return True
    else:
        return False


def is_date(date):
    date_formats = ["%d-%m-%Y", "%Y.%m.%d"]
    number_of_possible_exception = len(date_formats)
    exception_occured = 0
    for date_format in date_formats:
        try:
            result = strptime(date, date_format)
        except ValueError:
            exception_occured = exception_occured + 1

    if number_of_possible_exception == exception_occured:
        return False
    else:
        return True


def is_phone(phone):
    remove_spaces_str = sub('[\s+]', '', phone)

    phone_pattern = compile("^((\+7)+([0-9]){10})$")
    if phone_pattern.match(phone) != None:
        return True
    else:
        return False


if __name__ == '__main__':
    app.run()
